#include <iostream>
#include "student.h"

void is_set(bool check)
{
    if (check)
        std::cout << "Value assigned!\n";
    else
        std::cout << "Invalid Entry!\n";
}
int main()
{
    std::cout << "\n---------------------------------------\n";
    student stdt;
    std::string roll_no;
    int sub_marks;
    std::cout << "\n---------------------------------------\n";
    student stdt1("17i0544", 23, 3.46);
    std::cout << "\n---------------------------------------\n";

    std::cout << "Setting parameters through functions\n";
    bool check;
    stdt.set_roll_no("17i-0447");
    check = stdt.set_age(0);
    is_set(check);
    check = stdt.set_age(21);
    is_set(check);
    check = stdt.set_cgpa(-1);
    is_set(check);
    check = stdt.set_cgpa(3.28);
    is_set(check);

    std::cout << "Roll_no\t " << stdt.get_roll_no() << std::endl;
    std::cout << "Age\t " << stdt.get_age() << std::endl;
    std::cout << "cgpa\t " << stdt.get_cgpa() << std::endl;

    std::cout << "\n---------------------------------------\n";
    std::cout << "No subject marks added yet!\n";
    std::cout << "Printing All subjects marks\n";

    stdt.print_all_marks();

    std::cout << "\n---------------------------------------\n";
    std::cout << "Searching marks of subject programing\n";

    sub_marks = stdt.get_subject_marks("programing");
    if (sub_marks >= 0)
        std::cout << "programing\t " << sub_marks << std::endl;
    else
        std::cout << "programing "
                  << " not found!" << std::endl;

    stdt.set_subject_marks("programing", 99);
    std::cout << "Marks of subject programming added\n";
    sub_marks = stdt.get_subject_marks("programing");
    if (sub_marks >= 0)
        std::cout << "programing\t " << sub_marks << std::endl;
    else
        std::cout << "programing "
                  << " not found!" << std::endl;

    std::cout << "\n---------------------------------------\n";
    std::cout << "Some subjects marks added\n";
    stdt.set_subject_marks("Mathematics", 50);
    stdt.set_subject_marks("chemistry", 50);

    std::cout << "Printing All subjects marks\n";

    stdt.print_all_marks();
    std::cout << "\n---------------------------------------\n";

    std::cout << "Erasing programing subject\n";
    std::cout << "Printing All subjects marks\n";
    stdt.erase_subject_marks("programing");
    stdt.print_all_marks();

    std::cout << "\n---------------------------------------\n";

    

    return 0;
}

// int main()
// {
//     // int class_size;
//     // class_size = 3;
//     // int exist_stud_size = 0;
//     // char select;
//     // student stdt[class_size];
//     // do
//     // {
//     //     std::cout << "\n1. Add Student data\n2. Search student data\n"
//     //               << "Anyother key to exit\n"
//     //               << "Enter a number: ";
//     //     std::cin >> select;

//     //     switch (select)
//     //     {
//     //     case '1':
//     //         if (exist_stud_size < 3)
//     //         {
//     //             std::string roll_no;
//     //             int age;
//     //             float cgpa;
//     //             std::cout << "Enter Roll Number of student: ";
//     //             std::cin >> roll_no;
//     //             stdt[exist_stud_size].set_roll_no(roll_no);
//     //             std::cout << "Enter Age of student: ";
//     //             std::cin >> age;
//     //             stdt[exist_stud_size].set_age(age);
//     //             std::cout << "Enter Cgpa of student: ";
//     //             std::cin >> cgpa;
//     //             stdt[exist_stud_size].set_cgpa(cgpa);
//     //             ++exist_stud_size;
//     //         }
//     //         else
//     //         {
//     //             std::cout << "Memory full!\n";
//     //         }
//     //         break;
//     //         case '2':
//     //             std::string roll_no;
//     //             std::cout << "Enter Roll Number of student: ";
//     //             std::cin >> roll_no;
//     //             for(int index=0;index<exist_stud_size;index++)
//     //             {
//     //                 if(roll_no==stdt[index].get_roll_no())
//     //                 {
//     //                     std::cout << "";
//     //                 }
//     //             }

//     //         break;

//     //     default:
//     //         select = 'e';
//     //     }

//     // } while (select != 'e');
// }
