#include <iostream>
#include "student.h"
#include <iterator>

student::student(std::string new_roll_no, int new_age, float new_cgpa)
{
    std::cout << "Parametrized constructor called\n";
    data.roll_no = new_roll_no;
    data.age = new_age;
    data.cgpa = new_cgpa;

    std::cout << "Roll Number : " << data.roll_no << std::endl;
    std::cout << "Age : " << data.age << std::endl;
    std::cout << "CGPA : " << data.cgpa << std::endl;
}

student::student()
{
    std::cout << "Default constructor called\n";
    data.roll_no = "Nil";
    data.age = -1;
    data.cgpa = -1;
    std::cout << "Roll Number : " << data.roll_no << std::endl;
    std::cout << "Age : " << data.age << std::endl;
    std::cout << "CGPA : " << data.cgpa << std::endl;
}

student::~student()
{
    std::cout << "destructor called\n";
}

bool student::set_subject_marks(std::string subject, int marks)
{
    if (marks >= 0 && marks <= 100)
    {
        result[subject] = marks;
        return true;
    }
    else
        return false;
}

int student::get_subject_marks(std::string subject)
{
    if (result.find(subject) != result.end())
        return result.find(subject)->second; // overhead search
    else
        return -1;
}

void student::print_all_marks()
{
    std::map<std::string, int>::iterator it;
    std::cout << "subject marks of Roll_no " << data.roll_no << " is given below" << std::endl;
    std::cout << "---------------------------------------\n";
    std::cout << "Subject\t\tmarks" << std::endl;
    if (result.empty())
    {
        std::cout << "No data found" << std::endl;
        return;
    }
    for (it = result.begin(); it != result.end(); ++it)
    {
        std::cout << it->first;
        std::cout << '\t' << it->second << '\n'
                  << std::endl;
    }
}

std::string student::get_roll_no() const
{
    return data.roll_no;
}

void student::set_roll_no(const std::string &new_roll_no)
{
    data.roll_no = new_roll_no; // const reference
}

int student::get_age() const
{
    return data.age;
}

bool student::set_age(const int &new_age)
{
    if (new_age >= 4 && new_age <= 45)
    {
        data.age = new_age;
        return true;
    }
    else
        return false;
}

float student::get_cgpa() const
{
    return data.cgpa;
}

bool student::set_cgpa(float new_cgpa)
{
    if (new_cgpa >= 0 && new_cgpa <= 100)
    {
        data.cgpa = new_cgpa;
        return true;
    }
    else
        return false;
}

bool student::erase_subject_marks(std::string subject)
{
    if (result.find(subject) != result.end())
    {
        result.erase(subject);
        return true;
    }
    else
        return false;
}
