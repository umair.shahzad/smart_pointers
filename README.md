# cpp_new_project

## Initial Project Setup
Made directories **inlcude** **src** **app**.

Configured the **student.h** with the remaining function definitions and put this file in **include** directory.

Implemented all the functions in **student.cpp** and moved this in **src** directory 

Wrote a main.cpp file to check whether the functions implemented in **include/student.cpp** are working or not.

Moved **main.cpp** file in **app** directory

Made 3 **CMakeLists.txt** **src/CMakeLists.txt** and **include/CMakeLists.txt**.

For debug and release modes use **cmake -DCMAKE_BUILD_TYPE = type ..**. Type can be **Debug** **Release** and 2 more options are there.

## Separate the Declaration of Class from Implementation
Configured **inlcude/student.h** only for declaration of function and student class.

Implemented all the functions in **src/student.cpp** of **inlcude/student.h**. 

Made static library of class student.cpp naming **data_student** and linked it to executable **student** .

## Installation
clone the repostiory in your local system.

Get in the repository. Make a **build** directory and go in build. 

Inside build directory call the command **cmake ..**

Call **make** in the same respository and the executable file will build in the build/app directory.

To run the executable call **./app/student** or go in **build/app** and call **./student**
